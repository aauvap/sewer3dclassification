import argparse
import os
import glob
import h5py
from tqdm import tqdm
import numpy as np

def get_filepaths(pathdir):
    filepaths = glob.glob(f'{pathdir}/**/*.csv', recursive=True)
    filepaths.extend(glob.glob(f'{pathdir}/**/*.CSV', recursive=True))
    return filepaths

def get_class_folders(path):
    folders_total = glob.glob(f'{path}/**/', recursive=True)
    folders_with_csvs = []

    for folder in folders_total:
        files = glob.glob(f'{folder}/*.csv',recursive=False)
        files.extend(glob.glob(f'{folder}/*.CSV',recursive=False))
        if len(files) > 0:
            folders_with_csvs.append(folder)
    folders_with_csvs.reverse()
    return folders_with_csvs

def get_data(dataset_path):
    class_folders = get_class_folders(dataset_path)

    point_clouds_array = []
    labels_array = []

    for class_idx, c_folder in enumerate(class_folders):
        filepaths = get_filepaths(c_folder)
        print(f'Folder: {c_folder}')
        for f_path in tqdm(filepaths):
            point_cloud = np.genfromtxt(f_path, delimiter=',', dtype=np.float32)
            point_clouds_array.append(point_cloud)
            label = np.array(class_idx, dtype=np.int64)
            labels_array.append(np.array(label))

    temp_zip = list(zip(point_clouds_array, labels_array))
    np.random.shuffle(temp_zip)
    point_clouds_array, labels_array = zip(*temp_zip)
    return point_clouds_array, labels_array

def main(args):
    with h5py.File(f'{args.dataset_path}/{args.input_mode}_pointcloud_hdf5.h5','w') as hdf5:
        if args.input_mode == 'training':
            # Groups
            training_group = hdf5.create_group('Training')
            validation_group = hdf5.create_group('Validation')

            # Data
            point_clouds, labels = get_data(args.dataset_path)
            size = int(len(point_clouds)*args.val_ratio)

            # Training
            point_cloud_group = training_group.create_dataset('PointClouds', data=point_clouds[:-size])
            label_group = training_group.create_dataset('Labels', data=labels[:-size])

            # Validation
            point_cloud_group = validation_group.create_dataset('PointClouds',data=point_clouds[-size:])
            label_group = validation_group.create_dataset('Labels', data=labels[-size:])

        elif args.input_mode == 'testing':
            # Groups
            training_group = hdf5.create_group('Testing')

            # Data
            point_clouds, labels = get_data(args.dataset_path)
            size = int(len(point_clouds)*args.val_ratio)

            # Testing
            point_cloud_group = training_group.create_dataset('PointClouds', data=point_clouds[:-size])
            label_group = training_group.create_dataset('Labels', data=labels[:-size])


    with h5py.File(f'{args.dataset_path}/{args.input_mode}_pointcloud_hdf5.h5','r') as hdf5:
        for key in hdf5.keys():
            print(f'Dataset: {key}')
            print(list(hdf5.get(f'{key}/')))
            print(hdf5.get(f'{key}/Labels'))
            print(hdf5.get(f'{key}/PointClouds'))
    print('HDF5 Convertion is Complete!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Settings for CSV to HDF5 convertion.')
    parser.add_argument('--dataset_path', type=str, default='', metavar='N',
                        help='Path of the dataset.')
    parser.add_argument('--input_mode', type=str, default='training', metavar='N',
                        help='Type of dataset [training or test].')
    parser.add_argument('--val_ratio', type=float, default=0.2, metavar='N',
                        help='Validation ratio [0.0,1.0].')

    args = parser.parse_args()
    main(args)