import os
import pandas as pd
import numpy as np
from tqdm import tqdm
import glob
import random
import open3d as o3d
import csv


def get_filepaths(folder):
    """
    Recursively get all CSV- and PLY file paths from specified folder.

    Parameters:
        folder (string): Desired folder to look for paths.

    Returns:
        file_paths(list): List of all found CSV- and PLY paths paths.
    """
    file_paths = glob.glob(f'{folder}/**/*.csv', recursive=True)
    file_paths.extend(glob.glob(f'{folder}/**/*.CSV', recursive=True))
    file_paths.extend(glob.glob(f'{folder}/**/*.ply', recursive=True))
    return file_paths


def get_number_of_points(file_path):
    """
    Reads CSV file (point cloud) and counts number of rows (points).

    Parameters:
        file_path (string): Path to a CSV (Point Cloud).

    Returns:
        points_count(int): Number of rows (Points) found in CSV (Point Cloud).
    """
    points_count = 0
    with open(file_path) as f:
        reader = csv.reader(f, delimiter=',')
        points_count = sum(1 for row in reader)
        return points_count

def subsampling_algorithm(main_folder, algorithm, temp_parameter_value):
    """
    Uses CloudCompare to subsample point clouds.

    Parameters:
        main_folder (string): Path to main folder of the dataset.
        algorithm (string): Name of the subsampling algorithm from CloudCompare.
        parameter_value (int/float): Parameter is based upon algorithm.
            If SPATIAL, parameter is minimum distance between points.
            If RANDOM, parameter is a fixed number that should be subsampled to.

    """

    os_command = ''
    if os.name == 'nt': # Windows
        os_command = 'CloudCompare'
    elif os.name == 'posix': # Linux
        os_command = 'cloudcompare.CloudCompare'

    non_subsampled_filepaths = get_filepaths(main_folder)
    for filepath in non_subsampled_filepaths:
        if algorithm == 'SPATIAL':
            parameter_value = random.uniform(temp_parameter_value, temp_parameter_value + 0.01)
            while(True):
                os.system(f'{os_command} -SILENT -O {filepath} -AUTO_SAVE OFF -C_EXPORT_FMT ASC -EXT CSV -PREC 6 -SEP COMMA -SS SPATIAL {parameter_value} -SOR 60 2.00 -SAVE_CLOUDS FILE "{filepath}_done.csv"') 
                
                # Check if path exists, the subsampling method doesn't save a new file if it can't subsample.
                point_count = 0
                # if os.path.exists('{filepath}_done.csv'):
                point_count = get_number_of_points(f'{filepath}_done.csv')
                # else: 
                #     os.remove(filepath)
                #     parameter_value -= 0.004
                #     continue

                # Lower the threshold and try and subsample again.
                if point_count < 1024:
                    os.remove(f"{filepath}_done.csv")
                    parameter_value -= 0.004
                else:
                    os.remove(filepath)
                    break
        elif algorithm == 'RANDOM':
            os.system(f'{os_command} -SILENT -O {filepath} -AUTO_SAVE OFF -C_EXPORT_FMT ASC -EXT CSV -PREC 6 -SEP COMMA -SS RANDOM {temp_parameter_value} -SAVE_CLOUDS FILE "{filepath}_done.csv"') 
            os.remove(filepath)

def delete_pointclouds_below_limit(main_folder, limit):
    """
    Discards point clouds that are below the limit of desired points. 

    Parameters:
        main_folder (string): Path to main folder of the dataset.
        limit (int): The limit of points desired for a point cloud.
    """

    if not input_function('Delete pointclouds containing less than 1024 points? (y/n): '):
        return

    print('[INFO] Deleting Pointclouds below limit...')
    filepaths = get_filepaths(main_folder)
    for file in tqdm(filepaths):
        old_f = pd.read_csv(f'{file}')
        if len(old_f)+1 < limit:
            os.remove(file)


def delete_defective_pcs_without_defective_points(main_folder):
    """
    Discards defective point clouds that doesn't contain any defective points as these can be lost during subsampling. This is only done for synthetic datasets.
    This is to ensure that deep learning models train on defective point clouds that looks like normal point clouds.

    Parameters:
        main_folder (string): Path to main folder of the dataset.
    """

    if not input_function('Delete pointclouds containing no defected points? (y/n): '):
        return
    
    print('[INFO] Deleting Defective Pointclouds that have lost all their defective points...')
    filepaths = get_filepaths(main_folder+'/Defect')
    for file in tqdm(filepaths):
        csv = pd.read_csv(f'{file}')
        if any(csv.iloc[:, 3] == 1) != True:
            os.remove(file)


def remove_label_column(main_folder):
    """
    Synthetic point clouds contain a fourth column which contains labels [0(Defective)/1()] for each point. This is only done for synthetic datasets.
    """

    if not input_function('Remove label for each point in point clouds? (y/n): '):
        return

    print('[INFO] Remove label column...')
    filepaths = get_filepaths(main_folder)

    for file in tqdm(filepaths):
        try:
            old_f = pd.read_csv(f'{file}')
            new_f = old_f.drop(old_f.columns[3], axis=1)
            new_f.to_csv(f'{file}', index=False)
        except:
            print(file)


def delete_til_desired(main_folder):
    if not input_function('Delete point clouds until a specified number? (y/n): '):
        return

    folders_total = glob.glob(f'{main_folder}/**/', recursive=True)
    folders_with_csvs = []
    print(folders_total)

    for folder in folders_total:
        files = glob.glob(f'{folder}/*.csv',recursive=False)
        files.extend(glob.glob(f'{folder}/*.CSV',recursive=False))
        files.extend(glob.glob(f'{folder}/*.ply',recursive=False))
        if len(files) > 0:
            folders_with_csvs.append(folder)
            print(f'{folder} contains {len(files)} point clouds.')

    for folder in folders_with_csvs:
        desired_number = input(f'How many point clouds for {folder}?: ')
        paths = os.listdir(folder)
        random.shuffle(paths)

        if (input_function(f'Are you sure you want to delete {len(paths)-int(desired_number)} point clouds? (y/n): ') == False):
            continue

        for i in range(int(desired_number),len(paths)):
            os.remove(f'{folder}/{paths[i]}')


def input_function(message):
    answer = input(message).lower()
    while(True):
        if answer == 'y':
            return True
        elif answer == 'n':
            return False
        else:
            answer = input(message).lower()


def filter_zero_point(pointcloud, pointcloudNP, fileName, destinationPath):
    count = 0
    filter_arr = []
    for point in pointcloudNP:
        if np.all(point == 0):
            filter_arr.append(False)
            count = count+1
        else:
            filter_arr.append(True)
    pointcloudNParrayFiltered = pointcloudNP[filter_arr]

    pointcloud.points = o3d.utility.Vector3dVector(pointcloudNParrayFiltered)
    o3d.io.write_point_cloud(fileName, pointcloud)


def filter_zero_point_folder(dataPath, destinationPath):
    filepaths = get_filepaths(main_folder)

    for filepath in filepaths:
        pcd = o3d.io.read_point_cloud(f'{filepath}')
        pcdNP = np.asarray(pcd.points)
        filter_zero_point(pcd, pcdNP, filepath, destinationPath)
        print(rf'Saved {filepath}')


if __name__ == '__main__':
    main_folder = input('Path to dataset: ')
    synthetic_or_physical = input('Type of dataset [synthetic, physical]: ').lower()
    cloud_compare_folder = input('Path to CloudCompare: ')
    os.chdir(cloud_compare_folder)

    delete_til_desired(main_folder)

    if synthetic_or_physical == 'physical':
        filter_zero_point_folder(main_folder, main_folder)

    subsampling_algorithm(main_folder, 'SPATIAL', 0.03)
    subsampling_algorithm(main_folder, 'RANDOM', 1024)

    delete_pointclouds_below_limit(main_folder, 1024)

    if synthetic_or_physical == 'synthetic':
        delete_defective_pcs_without_defective_points(main_folder)
        remove_label_column(main_folder)

    print('Subsampling Complete!')
