# Multi-class Classification of 3D Sewer Defects

## 1) Generate point cloud dataset through Unity
To generate point clouds using Unity, open the Unity project folder _unity/UnityProject_: 

- Open the Unity Project in Unity and select SewerGenerationScene.
- Set display size to 224 x 171 px.
- Press the play icon to start the system.
- Select dictionary where the dataset should be generated.
- Choose number of normal- and defective pointclouds to be generated.
- Press generate.

## 2) Preprocess point cloud using subsampling
Before preprocessing, make a backup of the original dataset as the dataset will be overwritten. To subsample the generated point clouds: 

- Run _preprocessing/subsampling.py_.
- Follow the given instructions:
    - Path to where the dataset is generated. 
    - Select which type of dataset it is. 
    - Path to CloudCompare directory.
    - Delete point clouds containg less than 1024 points. This is desired due to classification requirements with a fixed size of 1024 point for each point cloud.
    - (Synthetic Type Only) Delete point clouds containing no defected points. This can be desired as there is a risk that all point that represents defective point clouds have been removed due to subsampling. 
    - (Synthetic Type Only) Remove label from each point in point cloud. This is desired when classifying. However, keeping these labels can be used for visualisation in e.g. CloudCompare.
    - Delete point clouds until a specified number. This can be used for specifying how many point clouds that is desired for each category.

## 3) Convert CSVs to HDF5
After preprocessing the dataset it is desired to convert all CSVs to an HDF5:

- Run _preprocessing/convert_csv_hdf5.py_
- Follow the given instructions:
    - Choose which dataset to generate.
- Move the generated HDF5 file into the folder _pointcloud_classification/data_.

## 4) Train deep learning model
Now the dataset is ready to used for training a deep learning model using _pointcloud_classification/data/training_pointcloud_hdf5.h5_:

- Run _pointcloud_classification/train_cls.py_
- Follow the given instructions:
    - Choose which archiecture to utilize. (Hyperparameters can be choosen in _pointcloud_classification/train_cls.py_)
- After the training, information and the models with the best validation accuracy and loss are saved into _pointcloud_classification/runs/experiment_name_

## 5) Test deep learning model
After training the deep learning model it can now be trained using _pointcloud_classification/data/testing_pointcloud_hdf5.h5_:

- Run _pointcloud_classification/test_cls.py_
- Follow the instructions:
    - Choose which iteration folder in the directory _pointcloud_classification/runs/experiment_name_.
- Information is logged into _metrics.txt_ and _cm.txt_ files in the specified experiment folder.

## The AAU Sewer Defect Point Cloud Dataset
The dataset used in the paper is available at: [Kaggle](https://www.kaggle.com/aalborguniversity/sewerpointclouds)

The dataset consists of the real life data collected in the laboratory environemnet at Aalborg University, as well as the generated synthetic data used in the paper.


### Code references
The PointNet code is based on the implementation of [Fei Xia](https://github.com/fxia22/pointnet.pytorch).

The DGCNN code is based on the implementation of [original authors](https://github.com/WangYueFt/dgcnn).


### License
All code is licensed under the MIT license, except for the aforementioned code reference, which are subject to their respective licenses when applicable. License restrictions from Unity still applies.

### Acknowledgements
Please cite the following paper if you use our code or dataset:

```TeX
@InProceedings{Haurum_2021_VISAPP,
author = {Haurum, Joakim Bruslund and Allahham, Moaaz M. J. and Lynge, Mathias S. and Henriksen, Kasper Sch{\o}n and Nikolov, Ivan A. and Moeslund, Thomas B.},
booktitle = {Proceedings of the 16th International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications - VISAPP},
title = {Sewer Defect Classification using Synthetic Point Clouds},
year = {2021},
publisher = {SciTePress},
organization={INSTICC}
} 
```