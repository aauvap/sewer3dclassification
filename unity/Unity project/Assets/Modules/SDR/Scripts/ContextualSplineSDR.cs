﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ContextualSpline takes the values from global parameters and instantiates objects based on the controlpoint informations.
/// Hieracy: ScenarioSDR --> GlobalParameters --> (ContextualSpline) --> Objects
/// </summary>
public class ContextualSplineSDR : StructuredDomainRandomization{

    ///Global variables to be accessed throughout the script. These variables are modified from the GlobalParameters using the Setup function. 
    public int numberOfControlPoints;
    public ControlPoint[] controlPoints;
    public bool contextIsDefect;
    public float defectProbability;
    public float splineType; //0 = Middle, 1 = Ceiling, 2 = floor

    ///Possible defect objects that can be accessed in this script. The objects are shown in the editor.
    public GameObject[] defectRubberRings;
    public GameObject[] defectBricks;

    public GameObject displacedCollider;

    /// <summary>
    /// This function is accessed by the GlobalParameters class and passes the following values;
    /// The number of controlpoints of the selected scenario.
    /// The array of controlpoint struct, which includes information shown in "StructureDomainRandomization" class. 
    /// Wether the Contextual spline is a defect.
    /// And the probability of having a defect, which is also from the selected scenario.
    /// </summary>
    public void Setup(int splineType, int numberOfControlPoints, ControlPoint[] controlPoints, bool contextIsDefect, float defectProbability){
        ///The following lines saves the values from the parameters into the global parameters.
        this.numberOfControlPoints = numberOfControlPoints; 
        this.controlPoints = controlPoints;
        this.contextIsDefect = contextIsDefect;
        this.defectProbability = defectProbability;
        this.splineType = splineType; //0 = Middle, 1 = Ceiling, 2 = floor

        Random.seed = (int)System.DateTime.Now.Ticks; /// Since random is never random, a new seed for the random generator is calculated using the time of the computer to enhance randomization.
        CreateObjects();
    }

    ///////////////////////
    // Objects
    //////////////////////

    /// <summary>
    /// Creates objects based on the context of the spline
    /// </summary>
    private void CreateObjects(){
        switch(splineType){
            case 0: //Middle Spline
                CreateSewerSystem();
                CreateDefect(displacedCollider,0.5f);
                break;
            case 1: //Ceiling Spline
                CreateDefect(defectRubberRings,0.5f);
                break;
            case 2: //Floor Spline
                CreateDefect(defectBricks, 0.085f);
                break;
        }
    }

    /// <summary>
    /// If context is not defect, then it create the sewer system, which consists of the pipes and the endblock to cover the camera from seeing outside of the sewer system.
    /// </summary>
    private void CreateSewerSystem(){
        for (int i = 0; i < numberOfControlPoints; i++) {
             ///If a controlpoint doesn't contain a pipe, then it will not go through the following code.
             ///If so, it will instantiate the given pipe prefab and then set its parent to an object container of this contextual spline.
            if (controlPoints[i].pipe != null) {
                GameObject newGameObject = Instantiate(controlPoints[i].pipe);
                newGameObject.transform.parent = this.transform.Find("Objects");
                
            }
            ///If a controlpoint doesn't contain a endblock, then it will not go through the following code.
            ///If so, it will instantiate the given endblock prefab and then set its parent to an object container of this contextual spline.
            if (controlPoints[i].endBlock != null) {
                GameObject newGameObject = Instantiate(controlPoints[i].endBlock);
                newGameObject.transform.parent = this.transform.Find("Objects");
            }
        }
    }


    private void CreateDefect(GameObject prefab, float defectProbability){
        for (int i = 0; i < numberOfControlPoints; i++){
            if (defectProbability > Random.Range(0f,1f)){
                GameObject newObject = Instantiate(prefab);
                newObject.GetComponent<DefectInfo>().id = newObject.tag+i;
                newObject.transform.parent = this.transform.Find("Objects");
                switch(newObject.tag){
                    case "Displacement":
                        CreateDisplacementColliders(controlPoints[i], newObject);
                        break;
                    case "RubberRing":
                        CreateRubberRings(controlPoints[i], newObject);
                        break;
                    case "Brick":
                        CreateBricks(controlPoints[i], newObject);
                        break;
                }
            }
        }
    }

    private void CreateDefect(GameObject[] prefab, float defectProbability){
        for (int i = 0; i < numberOfControlPoints; i++){
            if (defectProbability > Random.Range(0f,1f)){
                GameObject newObject = Instantiate(prefab[Random.Range(0,prefab.Length)]);
                newObject.GetComponent<DefectInfo>().id = newObject.tag+i;
                newObject.transform.parent = this.transform.Find("Objects");
                switch(newObject.tag){
                    case "Displacement":
                        CreateDisplacementColliders(controlPoints[i], newObject);
                        break;
                    case "RubberRing":
                        CreateRubberRings(controlPoints[i], newObject);
                        break;
                    case "Brick":
                        CreateBricks(controlPoints[i], newObject);
                        break;
                }
            }
        }
    }


    private void CreateDisplacementColliders(ControlPoint controlPoint, GameObject newObject){
        if (controlPoint.displacedPipe == true){ 
            Transform pipehead = controlPoint.pipe.transform.Find("Head").parent;
            newObject.transform.position = pipehead.position;
            newObject.transform.rotation = pipehead.rotation;
            newObject.transform.Rotate(new Vector3(180, 0, 0), Space.Self);
            newObject.transform.Translate(new Vector3(0, 0, -0.05f), Space.Self);
            newObject.transform.localScale = new Vector3(21f, 21f, 38f);
        }else{
            Object.Destroy(newObject);
        }
    }

    private void CreateBricks(ControlPoint controlPoint, GameObject newObject){
        newObject.transform.position = controlPoint.position;
        newObject.GetComponent<Rigidbody>().velocity = Random.onUnitSphere * 3;
    }

    /// <summary>
    /// If context is a defect, then it will create rubberrings based
    /// </summary>
    private void CreateRubberRings(ControlPoint controlPoint, GameObject newObject){
        if (controlPoint.displacedPipe == true || controlPoint.pipe == null){
            Object.Destroy(newObject);
            return;
        }
        
        Transform pipehead = controlPoint.pipe.transform.Find("Head").parent;
        newObject.transform.position = pipehead.position + new Vector3(0, 0, -0.05f);
        newObject.transform.rotation = pipehead.rotation;
        
        newObject.transform.Rotate(new Vector3(90, 0, 0), Space.Self);
        newObject.transform.Rotate(new Vector3(0, Random.Range(0f,360f), 0), Space.Self);
        newObject.transform.localScale = new Vector3(Random.Range(90f,110f),Random.Range(90f,110f),Random.Range(90f,110f));
    }   
}
