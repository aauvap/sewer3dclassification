﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSimulationSettings : MonoBehaviour{

    public int pixelsEachRayCovers = 1;
    public int imageWidth = 224;
    public int imageHeight = 171;

    public float rotationSpeed;
    public float translationSpeed;

    public float rotationRandomAmount;
    public float translationRandomAmount;

    void Start(){
        SimulationSettings.pixelsEachRayCovers = 1;
        SimulationSettings.imageWidth = 224;
        SimulationSettings.imageHeight = 171;
        SimulationSettings.rotationSpeed = rotationSpeed;
        SimulationSettings.translationSpeed = translationSpeed;
        SimulationSettings.rotationRandomAmount = rotationRandomAmount;
        SimulationSettings.translationRandomAmount = translationRandomAmount;
    }
}
