﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Contains information about the scene. And also given directory_path.
/// </summary>
public class SimulationSettings{
    public static int pixelsEachRayCovers;
    public static int imageWidth; 
    public static int imageHeight;
    public static string imagePath; 
    public static string directory_path;
    public static bool beginSimulation;

    public static float rotationSpeed;
    public static float translationSpeed;

    public static float rotationRandomAmount;
    public static float translationRandomAmount;
}
