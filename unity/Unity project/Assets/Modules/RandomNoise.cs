﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomNoise{

    /// <summary>
    /// Based upon Box-Muller: https://theclevermachine.wordpress.com/2012/09/11/sampling-from-the-normal-distribution-using-the-box-muller-transform/
    /// </summary>
    public static float Gaussian(float mean, float stdDev){
        float u1 = Random.Range(0.0f, 1.0f);
        float u2 = Random.Range(0.0f, 1.0f);
        float randStdNormal = Mathf.Sqrt(-2.0f * Mathf.Log(u1)) * Mathf.Cos(2.0f * Mathf.PI * u2);
        
        return Mathf.Abs(mean + stdDev * randStdNormal);
    }

    /// <summary>
    /// Used for debug purposes
    /// </summary>
    private static void SaveDataToCSV(float data){
        string path = string.Format("C:\\Users\\Schon\\Desktop\\gaussian.csv");
        using(System.IO.StreamWriter file = new System.IO.StreamWriter(path,true)){
            file.WriteLine(data);
        }
    }
}
