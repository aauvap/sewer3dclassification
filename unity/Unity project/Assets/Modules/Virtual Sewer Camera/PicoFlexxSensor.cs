﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PicoFlexxSensor : MonoBehaviour{
    public PointCloud pointCloud; ///To check if the point cloud is activated.
    Camera picoFlexCamera; ///Used for raycasting from each screen coordinate.
    Ray[] rayArray; ///An array consisting of all the rays.
    bool foundDefect; ///To indicate if an ray has found a defect.
    private DataGenerator dataGenerator; ///Used to access the datagenerator script, so it can begin generating images.
    public bool activateGenerateCSV; ///Boolean that can be accessed in the editor, to select generation of CSVs for 3D point clouds.
    public bool activateGenerateImages; //Boolean that can be accessed in the editor, to select generation of images for 2D data.
    Vector3[] directions; ///Directions towards where the raycast hits an object.
    bool unsureToLabelImageAsDefect; ///Not used anymore.
    public GameObject rayCubeInitializer; ///The Cube, which stops the issue for black hole, this one is disabled when everything is initialized.

    public GameObject SDR;

    private string defectType;

    List<string> defectiveObjects = new List<string>();

    void Start(){
        dataGenerator = this.transform.GetComponent<DataGenerator>();
        picoFlexCamera = this.transform.Find("Camera").GetComponent<Camera>(); ///Saves the camera component, duing it in start instead of update, increases the performance.
        rayArray = new Ray[(SimulationSettings.imageWidth/SimulationSettings.pixelsEachRayCovers)*(SimulationSettings.imageHeight/SimulationSettings.pixelsEachRayCovers)]; ///Size of the ray array
        foundDefect = false;
        directions = null;
        unsureToLabelImageAsDefect = true;
    }

    /// <summary>
    /// The struct Point, which contains information about each point used in the Point Cloud.
    /// </summary>
    public struct Point{
        public int defect;
        public Vector2 position;
        public Vector3 direction;
        public float distance;
        public Vector3 impactPoint;
    }

    void Update(){
        Point[] points = CastRaysAndObtainScreenPointPosition(); ///Get rays from each screen point and saves the screen point positions to an struct of arrays. 
        points = GetRayHitInformationAndFindDefect2(points); ///the array of point structs are passed, which saves the information of the position, direction and more about where the ray hits. Moreover, it checks if the ray hits a defect.

        rayCubeInitializer.SetActive(false); ///Disables the cube which used to initialize rays. Else the black circle will come.

        pointCloud.SetAllParticlesPositions(picoFlexCamera.transform.position,points); ///Sets the positions of the particles in the particle system.

        GenerateData(points); ///Generates data, either point clouds(csv) or images based on booleans (activateGenerateCSV and activateGenerateImages)
    }

    /// <summary>
    /// Casts rays towards each screen points and saves rays. Moreover, the screen point position is saved to the points struct. 
    /// Afterwards, an array of point structs are returned.
    /// </summary>
    private Point[] CastRaysAndObtainScreenPointPosition(){
        Point[] points = new Point[rayArray.Length];
        int indexCounter = 0;
        for(int y = 0; y<SimulationSettings.imageHeight/SimulationSettings.pixelsEachRayCovers;y++){
            for(int x = 0; x<SimulationSettings.imageWidth/SimulationSettings.pixelsEachRayCovers;x++){
                rayArray[indexCounter] = picoFlexCamera.ScreenPointToRay(new Vector3(x*SimulationSettings.pixelsEachRayCovers,y*SimulationSettings.pixelsEachRayCovers,-0.5f));
                points[indexCounter].position = new Vector2(x,y);
                indexCounter++;
            }
        }
        return points;
    }

    Point[] GetRayHitInformationAndFindDefect2(Point[] points){
        for(int i = 0; i<rayArray.Length;i++){
            
            RaycastHit[] hits;
            hits = Physics.RaycastAll(rayArray[i],4).OrderBy(h=>h.distance).ToArray();
            if(hits.Length == 0){ continue; }
            
            switch(hits[0].transform.tag){
                case "RubberRing": case "Brick": case "Displacement":
                    if(defectiveObjects.Contains(hits[0].transform.GetComponent<DefectInfo>().id) == false){
                        defectiveObjects.Add(hits[0].transform.GetComponent<DefectInfo>().id);
                    }
                    
                    foundDefect = true;
                    defectType = hits[0].transform.tag;
                    points[i].defect = 1;
                    points[i] = GetRayInformation(points[i],rayArray[i],hits[0]);

                    if(hits[0].transform.tag == "Displacement" && hits.Length > 1){
                        points[i] = GetRayInformation(points[i],rayArray[i],hits[1]);
                    }
                    break;
                default:
                    points[i].defect = 0;
                    points[i] = GetRayInformation(points[i],rayArray[i],hits[0]);
                    break;
            }
        }
        return points;
    }

    private Point GetRayInformation(Point point, Ray ray, RaycastHit hit){
        ///Saves the position of each ray hit
        point.direction = ray.direction;
        
        ///ImpactPoint is saved by subtracting the camera position to the position of where the raycast hits.
        point.impactPoint = transform.position - hit.point;

        ///Adds gaussian noise to the distance of the point
        float noise_distance_x = point.impactPoint.x * RandomNoise.Gaussian(0, 0.02f);
        float noise_distance_y = point.impactPoint.y * RandomNoise.Gaussian(0, 0.02f);
        float noise_distance_z = point.impactPoint.z * RandomNoise.Gaussian(0, 0.02f);

        Vector3 noise_vector = new Vector3(noise_distance_x, noise_distance_y, noise_distance_z);
        point.impactPoint = point.impactPoint+noise_vector;

        float distance = Vector3.Distance(transform.position,point.impactPoint);
        //float angle = Vector3.AngleBetween
        point.distance = distance;
        return point;
    }

    /// <summary>
    /// Generates data based on the active booleans from the editor
    /// </summary>
    private void GenerateData(Point[] points){
        if(!pointCloud.pointCloudIsReady){return;}
        
        if(activateGenerateImages){
            dataGenerator.GenerateImage(foundDefect, defectType);
        }

        if(activateGenerateCSV){
            if(defectiveObjects.Count < 2){
                dataGenerator.GenerateCSV(points,foundDefect, defectType);
            }else{
                print("More than 1 defective found");
            }
        }

        foundDefect = false;
        defectiveObjects.Clear();
        this.transform.GetComponent<MoveCamera>().UpdateCameraMovement(); ///Updates the camera position towards the controlpoints in the pipe contexual spline.
    }
}
